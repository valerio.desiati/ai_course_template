import json
import joblib
import locale
import numpy as np
import pandas as pd
from flask import Flask, request, render_template

app = Flask(__name__)

@app.route('/returnValueForMap', methods=['POST'])
def returnValueForMap():
    dataForMap = pd.read_csv("../../data/processed/features.csv")
    Lat = dataForMap['lat']
    Lon = dataForMap['lon']
    Prod = dataForMap['produzione_media']

    stringReturn = ""
    for x in Lat:
        stringReturn = stringReturn + str(x) + ","
    stringReturn = stringReturn + "#"
    for x in Lon:
        stringReturn = stringReturn + str(x) + ","
    stringReturn = stringReturn + "#"
    for x in Prod:
        stringReturn = stringReturn + str(x) + ","

    return stringReturn


# Carica le metriche dal file JSON
with open('../notebooks/model_metrics.json', 'r') as f:
    model_metrics = json.load(f)

# Route per visualizzare il form (default http://127.0.0.1:5000)
@app.route('/', methods=['GET'])
def form():
    return render_template('index.html')


@app.route('/predict', methods=['POST'])
def predict():
    try:
        # Ricezione i dati dal form HTML
        alt = float(request.form['alt'])
        lat = float(request.form['lat'])
        lon = float(request.form['lon'])
        superficie = float(request.form['superficie_media'])
        prec = float(request.form['prec_media'])
        temp = float(request.form['temp_media'])
        
        # Seleziona il modello basato sulla scelta dell'utente
        model = joblib.load('../../models/rf_harvest_model.pkl')
        if model is None:
            return "Errore: Modello non valido", 400

        # Si crea un dataframe da fornire al modello
        input_data = pd.DataFrame({
            'alt': [alt],
            'lat': [lat],
            'lon': [lon],
            'superficie': [superficie],
            'prec': [prec],
            'temp': [temp],
        })
        
        print(input_data)

        # Si recuperano le previsioni da ciascun albero con model.estimators_    
        prediction = np.array([tree.predict(input_data) for tree in model.estimators_])
        print(prediction)
        # Calcolo della media, del 5° percentile e del 95° percentile
        median_prediction = round(np.percentile(prediction, 50))
        lower_bound = round(np.percentile(prediction, 5))
        upper_bound = round(np.percentile(prediction, 95))

        # Imposta la localizzazione italiana
        locale.setlocale(locale.LC_ALL, 'it_IT.UTF-8')
        
        # Formatta il numero della previsione come "xxx.xxx"
        median_prediction = locale.format_string('%.0f', median_prediction, grouping=True)
        lower_bound = locale.format_string('%.0f', lower_bound, grouping=True)
        upper_bound = locale.format_string('%.0f', upper_bound, grouping=True)
        
        # Crea messaggio di testo formattato da restituire
        prediction_text = f'Previsione media di raccolto: {median_prediction} quintali. <br> Range: {lower_bound} - {upper_bound} quintali.'
        print(f'Previsione media di raccolto: {median_prediction} quintali. \nRange: {lower_bound} - {upper_bound} quintali.')
        
        # Restituisci il messaggio di testo formattato
        return prediction_text
   
    except Exception as e:
        # Gestione degli errori
        print(e)
        return f"Errore durante la previsione: {str(e)}", 500
    
if __name__ == '__main__':
    app.run(debug=True)