# Previsione del raccolto di frumento tenero nella regione Emilia Romagna

## Overview

Lo scopo di questo progetto è la creazione di un modello di regressione per la previsione dei raccolti di frumento tenero a partire da dati geografici (altitudine, latitudine e longitudine) del terreno, serie storiche (dal 2006 al 2021) di superfici coltivate, produzioni annue, misurazioni delle temperature e delle precipitazioni.

## Installazione

1. Clona la repository: `git clone https://gitlab.com/valerio.desiati/ai_course_template.git`.
2. Installa le dipendenze ed esegui l'app in Flask: `./start`.
3. Vai al link `https://127.0.0.1:5000` tramite browser.

## Fonti dei dati

- `data/raw/coltivazioni_istat.xlsx` [ISTAT](https://dati.istat.it/)
- `data/raw/comuni_alt.xlsx` [ISTAT](https://dati.istat.it/)
- `data/raw/comuni_lon_lat.xlsx` [GitHub](https://github.com/MatteoHenryChinaski/Comuni-Italiani-2018-Sql-Json-excel/blob/master/italy_geo.xlsx)
- `data/raw/elenco_comuni.xlsx` [Garda Informatica](https://www.gardainformatica.it/database-comuni-italiani#)
- `data/raw/elenco_province.xlsx` [Garda Informatica](https://www.gardainformatica.it/database-comuni-italiani#)
- `data/raw/temp_prec.xlsx` [ISTAT](https://dati.istat.it/)

## Struttura del progetto

- `/data`: Contiene dati grezzi e processati.
  - `/processed`: Dati processati.
    - `dataset_final.csv`: Dati finali non aggregati.
    - `features.csv`: Dati finali aggregati.
  - `/raw`: Dati grezzi.
    - `coltivazioni_istat.xlsx`: Dati relativi ai raccolti di frumento in Italia dal 2006 al 2021.
    - `dataset_frumento_prov_ita.csv`: Dati relativi ai raccolti di frumento in Italia dal 2006 al 2021 semi processati.
    - `comuni_alt.xlsx`: Altitudini delle città italiane.
    - `comuni_lon_lat.xlsx`: Latitudini e longitudini delle città italiane.
    - `comuni_geo.csv`: Geo dati delle città italiane (semi - processati, contiene altitudine, latitudine and longitudine).
    - `elenco_comuni.xlsx`: Lista delle città italiane.
    - `elenco_province.xlsx`: Lista delle province italiane.
    - `temp_prec.xlsx`: Dati relativi alle temperature e precipitazioni italiane dal 2006 al 2021.
    - `precipitazioni2006-2021.csv`: Dati pre - processati relativi alle precipitazioni italiane.
    - `temperature2006-2021.csv`: Dati pre - processati relativi alle temperature italiane.
- `/models`: Contiene i file di salvataggio dei modelli addestrati.
- `/src`: Codice sorgente del progetto.
  - `/scripts`: Script.
    - `/templates`: Interfaccia della web app Flask.
    - `app.py`: App Flask.
  - `/notebooks`: Notebook Jupyter.
    - `1-prep_coltivazioni.ipynb`: Preprocessing dei dati relativi ai raccolti.
    - `2-prep_temp_prec.ipynb`: Preprocessing dei dati relativi alle temperature e precipitazioni.
    - `3-prep_geo.ipynb`: Preprocessing dei dati geografici.
    - `4-final.ipynb`: Merge finale dei dati.
    - `5-train_model.ipynb`: Addestramento dei modelli con dati aggregati.
    - `5b-train_model_all_data.ipynb`: Addestramento dei modelli con dati non aggregati.

## Licenza

Creative Commons Zero v1.0 Universal.

## Contatti

Valerio Desiati - valerio.desiati@studio.unibo.it \
Lucrezia Ilvento - lucrezia.ilvento@studio.unibo.it \
Alberto Casado Moreno - alberto.casadomoreno@studio.unibo.it
